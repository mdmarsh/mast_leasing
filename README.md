# mast_leasing

## Report Requirements and Design Analysis

###Reports:

1. Produce a list sorted by Current Rent in ascending order . Obtain the first 5 items from the resultant list and  output to the console.

    **Index by Current Rent, sort ascending, take first 5 and list all fields. Index more efficiently produced on read.  Output = all fields list ordered by current rent (5 only)**

2. From the list of all mast data create new list of mast data with Lease Years = 25 years. Output the list to the console, include all data fields. Output the total rent for all items in this list to the console.

    **Possible use of Index  by Lease Years or just filter data by lease years no mention of order of output.   Output = all fields, unspecified order + total rent of 25 year leases**

3. Create a dictionary containing tenant name and a count of masts for each tenant. Output the dictionary to the console in a readable form. NOTE. Treat "Everything Everywhere Ltd" and "Hutchinson3G Uk Ltd&Everything Everywhere Ltd" as separate entities.

    ** Either use Index by tenant name sort and collated same names to dict or scan  all data and compile dict.   Output = list of tenant name and mask count, unspecified order**

4. List the data for rentals with Lease Start Date between 1 June 1999 and 31 Aug 2007.
   Output the data to the console with dates formatted as DD/MM/YYYY.

    **Possible index by Lease Start date or scan through data filtering on Lease start date. Output = all fields, unspecified order**

## Design overview

None of the possible indexes are re-used in tasks 1 to 4 and data set is small suggesting over use of indexes would not be advantageous as it would use more memory.
An index for requirement 1 would make sense as we avoid sorting all the data.

The use of command line and submission of git repro suggests that a simple python app would be all that is required.   Since Pycharm is mentioned in job spec and is also a personal preference I will create it to run and be tested in that IDE.  

The data is so small that re-reading and parsing it on every call is feasible but not elegant especially if the data file name is not hard coded.
A nice way to read in data in once and have simple commands would be to use Redis as an in memory data store.  Then simple commands could be run on the stored data. However, that requires installing Redis onto the user's system and complicates the test.  I have decided to use a simple console menu to get persistence by keeping data in memory. To get command line flexibility I have also used docopt library so there is POSIX standard interface to allow output and piping of one or more reports and suppression of menu at cost of reparsing the data.

## How to Use 

set up virtual enev as in requirements.txt
to run in pycharm set up edit configs to point to script eg:

   **run script from:   /home/USERNAME/Documents/mast_leasing/mobilelease/cli.py**
   
   **set script parameters: "../data/Mobile Phone Masts.csv"**

In pycharm other configs may be set up for producing reports
by using options see usage below

Should be also runnable by going to app directory in console after using "source ../venv/bin/activate"


Usage: cli.py [-h1234an] FILE ...

Use help option or give optional options and the CSV file containing data

if option n or --nomenu prevents menu being presented for run once with cleaner output
eg for piping etc otherwise after requested report/s the menu will be displayed.

When a menu is output; please enter the menu number to list the desired report or
option 0 to exit, option 5 to print all reports

###Arguments:
       FILE        needed CSV data file
###Options:
      -h --help    help file
      -1           output report 1
      -2           output report 2
      -3           output report 3
      -4           output report 4
      -a --all          output all reports
      -n --nomenu       no menu just exit when report printed (ideal for piping)



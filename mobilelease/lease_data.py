import csv
from operator import itemgetter
from datetime import datetime


class LeaseData(object):
    """Data model/ business logic

    The report data and table is stored in an instance of the this class
    This allows easy testing by examining self.report_data.
    To conserve space except for report 3 only ids of fields are prepared.
    Report 3 prepares the prescribed dictionary.

    """

    def __init__(self):
        self.data_table = {}
        self.index_current_rent = []
        self.report_data = None
        self.headers = []
        self.reports = [
            self.prepare_report_1,
            self.prepare_report_2,
            self.prepare_report_3,
            self.prepare_report_4
        ]
        self.date_format = "%d %b %Y"

    def read_csv(self, file_name):
        """read and parse data file

        :param file_name: link to csv data file
        :return: none

        """
        with open(file_name, newline='') as csvfile:
            data_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            row_id = 0

            for row in data_reader:
                if row_id == 0:
                    self.headers = row
                else:
                    row.reverse()
                    data_row = {}
                    for header in self.headers:
                        data_row[header] = row.pop()
                    self.data_table[row_id] = data_row
                    self.index_current_rent.append((row_id, float(data_row['Current Rent'])))
                row_id += 1

    def prepare_report(self, n):
        """report dispatch

        :param n: report number required
        :return: none

        """
        n -= 1
        if 0 <= n <= 3:
            self.reports[n]()

    def prepare_report_1(self):
        """report 1
        Produce a list sorted by Current Rent in ascending order.
        Obtain the first 5 items from the resultant list and output to the console.
        The first 5 are selected on print

        prepares list of matching data table ids in self.report_data
        :return: none

        """
        self.report_data = [x for x, _ in sorted(self.index_current_rent, key=itemgetter(1))]

    def prepare_report_2(self):
        """report 2
        From the list of all mast data create new list of mast data with Lease Years = 25 years.
        Output the list to the console, include all data fields.
        Output the total rent for all items in this list to the console calculated on print

        prepares list of matching data table ids in self.report_data

        :return: none

        """
        self.report_data = [x for x, _ in self.data_table.items() if int(self.data_table[x]['Lease Years']) == 25]

    def prepare_report_3(self):
        """report 3
        Create a dictionary containing tenant name and a count of masts for each tenant.
        Output the dictionary to the console in a readable form.
        NOTE. Treat "Everything Everywhere Ltd" and "Hutchinson3G Uk Ltd&Everything Everywhere
        Ltd" as separate entities.

        prepares dictionary in self.report_data

        :return: none

        """
        self.report_data = {}
        for index, data in self.data_table.items():
            tenant = data['Tenant Name']
            if self.report_data.get(tenant):
                self.report_data[tenant] += 1
            else:
                self.report_data[tenant] = 1

    def prepare_report_4(self):
        """report 4
        List the data for rentals with Lease Start Date between 1 June 1999 and 31 Aug 2007.

        prepares list of matching data table ids in self.report_data

        :return: none

        """
        self.report_data = []
        self.date_format = "%d %b %Y"
        start = datetime.strptime("01 Jun 1999", self.date_format)
        end = datetime.strptime("31 Aug 2007", self.date_format)
        for index, data in self.data_table.items():
            d1 = datetime.strptime(data['Lease Start Date'], self.date_format)
            if start <= d1 <= end:
                self.report_data.append(index)

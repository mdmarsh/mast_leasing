from datetime import datetime
from copy import copy

class LeaseReport(object):

    def __init__(self, data_instance):
        self.data = data_instance
        self.reports = [
            self.print_1,
            self.print_2,
            self.print_3,
            self.print_4
        ]
        self.full_format = "{:<40}{:<34}{:<23}{:<23}{:<23}{:<45}{:<45}{:>18}{:>18}{:>14}{:>14}"

    def print(self, n):
        """report dispatch

        :param n: report number required
        :return: none

        """
        n -= 1
        if 0 <= n <= 3:
            self.reports[n]()

    def print_1(self):
        """report 1
        Produce a list sorted by Current Rent in ascending order.
        Obtain the first 5 items from the resultant list and output to the console.
        The first 5 are selected on print

        prepares list of matching data table ids in self.report_data
        :return: none

        """
        print("\n\n-- Report 1 --\n ")
        self.header_out()
        for i in range(0, 5):
            ind = self.data.report_data[i]
            self.row_out(self.data.data_table[ind])

    def print_2(self):
        """report 2
        From the list of all mast data create new list of mast data with Lease Years = 25 years.
        Output the list to the console, include all data fields.
        Output the total rent for all items in this list to the console calculated on print

        prepares list of matching data table ids in self.report_data

        :return: none

        """
        print("\n\n-- Report 2 --\n ")
        self.header_out()
        total = 0
        for i in self.data.report_data:
            self.row_out(self.data.data_table[i])
            total += float(self.data.data_table[i]['Current Rent'])
        print("\n\nTotal Rent: {:>10}\n".format(total))

    def print_3(self):
        """report 3
        Create a dictionary containing tenant name and a count of masts for each tenant.
        Output the dictionary to the console in a readable form.
        NOTE. Treat "Everything Everywhere Ltd" and "Hutchinson3G Uk Ltd&Everything Everywhere
        Ltd" as separate entities.

        prepares dictionary in self.report_data

        :return: none

        """
        print("\n\n-- Report 3 --\n ")
        format_str = "{:<45}{:>8}"
        print(format_str.format("Tenant", "No. Masts"))
        print()
        for tenant, masts in self.data.report_data.items():
            print(format_str.format(tenant, masts))


    def print_4(self):
        """report 4
        List the data for rentals with Lease Start Date between 1 June 1999 and 31 Aug 2007.
        Output the data to the console with dates formatted as DD/MM/YYYY.

        Up to now the requirement has been to list the data as original format so
        no conversion on read was done. A reformating based on knowledge of date
        fields ia required.

        prepares list of matching data table ids in self.report_data

        :return: none

        """
        print("\n\n-- Report 4 --\n ")
        self.header_out()
        out_date_format = "%d/%m/%Y"
        for i in self.data.report_data:
            row = copy(self.data.data_table[i])
            ds = datetime.strptime(row['Lease Start Date'], self.data.date_format)
            de = datetime.strptime(row['Lease End Date'], self.data.date_format)
            row['Lease Start Date'] = ds.strftime(out_date_format)
            row['Lease End Date'] = de.strftime(out_date_format)
            self.row_out(row)

    def header_out(self):
        """

        Property Name,Property Address [1],Property  Address [2],Property Address [3],
        Property Address [4],Unit Name,
        Tenant Name,Lease Start Date,Lease End Date,Lease Years,Current Rent

        :return:
        """
        print(self.full_format.format(
            self.data.headers[0],
            self.data.headers[1],
            self.data.headers[2],
            self.data.headers[3],
            self.data.headers[4],
            self.data.headers[5],
            self.data.headers[6],
            self.data.headers[7],
            self.data.headers[8],
            self.data.headers[9],
            self.data.headers[10],
        ))
        print()

    def row_out(self, row):
        print(self.full_format.format(
            row[self.data.headers[0]],
            row[self.data.headers[1]],
            row[self.data.headers[2]],
            row[self.data.headers[3]],
            row[self.data.headers[4]],
            row[self.data.headers[5]],
            row[self.data.headers[6]],
            row[self.data.headers[7]],
            row[self.data.headers[8]],
            row[self.data.headers[9]],
            row[self.data.headers[10]],
        ))

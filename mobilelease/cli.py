#!/usr/bin/env python
"""
Usage: cli.py [-h1234an] FILE ...

Use help option or give optional options and the CSV file containing data

if option n or --nomenu prevents menu being presented for run once with cleaner output
eg for piping etc otherwise after requested report/s the menu will be displayed.

When a menu is output; please enter the menu number to list the desired report or
option 0 to exit, option 5 to print all reports

Arguments:
  FILE        needed CSV data file
Options:
  -h --help    help file
  -1           output report 1
  -2           output report 2
  -3           output report 3
  -4           output report 4
  -a --all          output all reports
  -n --nomenu       no menu just exit when report printed (ideal for piping)
"""
from docopt import docopt
from mobilelease.lease_data import LeaseData
from mobilelease.lease_report import LeaseReport


def main(args):
    # print(args)
    file_name = ""
    report_options = ['-1', '-2', '-3', '-4', '--all', '--nomenu']
    reports = {}
    data = LeaseData()
    lease_report = LeaseReport(data)

    for arg, value in args.items():
        if value:
            if arg == 'FILE' and len(value) == 1:
                file_name = value[0]

            else:
                reports[report_options.index(arg)+1] = 1

    if file_name:
        data.read_csv(file_name)
        menu_item = -1
        show_menu = not reports.get(6)

        while menu_item:

            if not reports and show_menu:
                print("\nReport Menu")
                print("0 exit")
                print("1 report one; 5 lowest rents")
                print("2 report two; 25yr leases")
                print("3 report three; count of masts by tenant")
                print("4 report four; rents from 1 June 1999 to 31 Aug 2007")
                print("5 all reports")

                try:
                    menu_item = int(input("Enter number to select menu item: "))
                except ValueError:
                    menu_item = -1
                if 0 <= menu_item <= 5:
                    reports[menu_item] = 1
                else:
                    print("Error: Enter number 0 to 5\n")

            if reports:
                for n in range(1, 5):
                    if reports.get(n) or reports.get(5):
                        data.prepare_report(n)
                        lease_report.print(n)

            if not show_menu:
                menu_item = 0

            reports = {}


if __name__ == '__main__':
    arguments = docopt(__doc__)
    main(arguments)

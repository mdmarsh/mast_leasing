from mobilelease.cli import main as cli_main
from unittest.mock import patch
from unittest.mock import call
import io
import sys
import unittest


class IoBaseTestCase(unittest.TestCase):

    def stub_stdin(self, inputs):
        self.std_in = sys.stdin

        def cleanup():
            sys.stdin = self.std_in

        self.addCleanup(cleanup)
        sys.stdin = io.StringIO(inputs)

    def stub_stdout(self):
        self.std_out = sys.stdout

        def cleanup():
            sys.stdout = self.std_out

        self.addCleanup(cleanup)
        sys.stdout = io.StringIO()


class TestCliCommands(IoBaseTestCase):

    def setUp(self):
        self.cli_args = {
                '--all': False,
                '--help': False,
                '--nomenu': False,
                '-1': False,
                '-2': False,
                '-3': False,
                '-4': False,
                'FILE': ['test.csv']}

    @patch('mobilelease.cli.LeaseReport')
    @patch('mobilelease.cli.LeaseData')
    def test_menu_0(self, mock_lease_data_class, mock_report_class):
        self.stub_stdout()
        self.stub_stdin('0')
        cli_main(self.cli_args)
        op = sys.stdout.getvalue().strip()
        self.assertEqual(op[0:11], 'Report Menu')
        self.assertEqual(op[-10:], 'menu item:')
        self.assertListEqual(mock_lease_data_class.mock_calls, [
            call(), call().read_csv('test.csv')
        ])
        mock_report_class.assert_has_calls([])

    @patch('mobilelease.cli.LeaseReport')
    @patch('mobilelease.cli.LeaseData')
    def test_menu_1(self, mock_lease_data_class, mock_report_class):
        self.stub_stdout()
        self.stub_stdin('1\n0')
        cli_main(self.cli_args)
        op = sys.stdout.getvalue().strip()
        self.assertEqual(op[0:11], 'Report Menu')
        self.assertEqual(op[-10:], 'menu item:')
        self.assertListEqual(mock_lease_data_class.mock_calls, [
            call(), call().read_csv('test.csv'), call().prepare_report(1)
        ])
        mock_report_class.assert_has_calls([
            call().print(1)
        ])

    @patch('mobilelease.cli.LeaseReport')
    @patch('mobilelease.cli.LeaseData')
    def test_menu_2(self, mock_lease_data_class, mock_report_class):
        self.stub_stdout()
        self.stub_stdin('2\n0')
        cli_main(self.cli_args)
        op = sys.stdout.getvalue().strip()
        self.assertEqual(op[0:11], 'Report Menu')
        self.assertEqual(op[-10:], 'menu item:')
        self.assertListEqual(mock_lease_data_class.mock_calls, [
            call(), call().read_csv('test.csv'), call().prepare_report(2)
        ])
        mock_report_class.assert_has_calls([
            call().print(2)
        ])

    @patch('mobilelease.cli.LeaseReport')
    @patch('mobilelease.cli.LeaseData')
    def test_menu_3(self, mock_lease_data_class, mock_report_class):
        self.stub_stdout()
        self.stub_stdin('3\n0')
        cli_main(self.cli_args)
        op = sys.stdout.getvalue().strip()
        self.assertEqual(op[0:11], 'Report Menu')
        self.assertEqual(op[-10:], 'menu item:')
        self.assertListEqual(mock_lease_data_class.mock_calls, [
            call(), call().read_csv('test.csv'), call().prepare_report(3)
        ])
        mock_report_class.assert_has_calls([
            call().print(3)
        ])

    @patch('mobilelease.cli.LeaseReport')
    @patch('mobilelease.cli.LeaseData')
    def test_menu_4(self, mock_lease_data_class, mock_report_class):
        self.stub_stdout()
        self.stub_stdin('4\n0')
        cli_main(self.cli_args)
        op = sys.stdout.getvalue().strip()
        self.assertEqual(op[0:11], 'Report Menu')
        self.assertEqual(op[-10:], 'menu item:')
        self.assertListEqual(mock_lease_data_class.mock_calls, [
            call(), call().read_csv('test.csv'), call().prepare_report(4)
        ])
        mock_report_class.assert_has_calls([
            call().print(4)
        ])

    @patch('mobilelease.cli.LeaseReport')
    @patch('mobilelease.cli.LeaseData')
    def test_menu_5(self, mock_lease_data_class, mock_report_class):
        self.stub_stdout()
        self.stub_stdin('5\n0')
        cli_main(self.cli_args)
        op = sys.stdout.getvalue().strip()
        self.assertEqual(op[0:11], 'Report Menu')
        self.assertEqual(op[-10:], 'menu item:')
        self.assertListEqual(mock_lease_data_class.mock_calls, [
            call(), call().read_csv('test.csv'), call().prepare_report(1),
            call().prepare_report(2), call().prepare_report(3), call().prepare_report(4)
        ])
        mock_report_class.assert_has_calls([
            call().print(1), call().print(2), call().print(3), call().print(4)
        ])

    @patch('mobilelease.cli.LeaseReport')
    @patch('mobilelease.cli.LeaseData')
    def test_cli_1(self, mock_lease_data_class, mock_report_class):
        self.cli_args['--nomenu'] = True
        self.cli_args['-1'] = True
        cli_main(self.cli_args)
        mock_lease_data_class.assert_has_calls([
            call(), call().read_csv('test.csv'), call().prepare_report(1)
        ])
        mock_report_class.assert_has_calls([call().print(1)])

    @patch('mobilelease.cli.LeaseReport')
    @patch('mobilelease.cli.LeaseData')
    def test_cli_2(self, mock_lease_data_class, mock_report_class):
        self.cli_args['--nomenu'] = True
        self.cli_args['-2'] = True
        cli_main(self.cli_args)
        mock_lease_data_class.assert_has_calls([
            call(), call().read_csv('test.csv'), call().prepare_report(2)
        ])
        mock_report_class.assert_has_calls([call().print(2)])

    @patch('mobilelease.cli.LeaseReport')
    @patch('mobilelease.cli.LeaseData')
    def test_cli_3(self, mock_lease_data_class, mock_report_class):
        self.cli_args['--nomenu'] = True
        self.cli_args['-3'] = True
        cli_main(self.cli_args)
        self.assertListEqual(mock_lease_data_class.mock_calls, [
            call(), call().read_csv('test.csv'), call().prepare_report(3)
        ])
        mock_report_class.assert_has_calls([call().print(3)])

    @patch('mobilelease.cli.LeaseReport')
    @patch('mobilelease.cli.LeaseData')
    def test_cli_4(self, mock_lease_data_class, mock_report_class):
        self.cli_args['--nomenu'] = True
        self.cli_args['-4'] = True
        cli_main(self.cli_args)
        self.assertListEqual(mock_lease_data_class.mock_calls, [
            call(), call().read_csv('test.csv'), call().prepare_report(4)
        ])
        mock_report_class.assert_has_calls([call().print(4)])

    @patch('mobilelease.cli.LeaseReport')
    @patch('mobilelease.cli.LeaseData')
    def test_cli_all(self, mock_lease_data_class, mock_report_class):
        self.cli_args['--nomenu'] = True
        self.cli_args['--all'] = True
        cli_main(self.cli_args)
        mock_lease_data_class.assert_has_calls([
            call(), call().read_csv('test.csv'),
            call().prepare_report(1), call().prepare_report(2), call().prepare_report(3),
            call().prepare_report(4)
        ])
        mock_report_class.assert_has_calls([
            call().print(1), call().print(2), call().print(3), call().print(4)
        ])

    @patch('mobilelease.cli.LeaseReport')
    @patch('mobilelease.cli.LeaseData')
    def test_cli_1234(self, mock_lease_data_class, mock_report_class):
        self.cli_args['--nomenu'] = True
        self.cli_args['-1'] = True
        self.cli_args['-2'] = True
        self.cli_args['-3'] = True
        self.cli_args['-4'] = True
        cli_main(self.cli_args)
        mock_lease_data_class.assert_has_calls([
            call(), call().read_csv('test.csv'),
            call().prepare_report(1), call().prepare_report(2), call().prepare_report(3),
            call().prepare_report(4)
        ])
        mock_report_class.assert_has_calls([
            call().print(1), call().print(2), call().print(3), call().print(4)
        ])

    @patch('mobilelease.cli.LeaseReport')
    @patch('mobilelease.cli.LeaseData')
    def test_cli_13ALL(self, mock_lease_data_class, mock_report_class):
        self.cli_args['--nomenu'] = True
        self.cli_args['-1'] = True
        self.cli_args['-3'] = True
        self.cli_args['--all'] = True
        cli_main(self.cli_args)
        mock_lease_data_class.assert_has_calls([
            call(), call().read_csv('test.csv'), call().prepare_report(1), call().prepare_report(2),
            call().prepare_report(3), call().prepare_report(4)
            ])
        mock_report_class.assert_has_calls([
            call().print(1), call().print(2), call().print(3), call().print(4)
        ])

    @patch('mobilelease.cli.LeaseReport')
    @patch('mobilelease.cli.LeaseData')
    def test_cli_13(self, mock_lease_data_class, mock_report_class):
        self.cli_args['--nomenu'] = True
        self.cli_args['-1'] = True
        self.cli_args['-3'] = True
        cli_main(self.cli_args)
        mock_lease_data_class.assert_has_calls([
            call(), call().read_csv('test.csv'), call().prepare_report(1),
            call().prepare_report(3),
            ])
        mock_report_class.assert_has_calls([
            call().print(1), call().print(3)
        ])


if __name__ == '__main__':
    unittest.main()

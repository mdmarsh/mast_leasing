from mobilelease.lease_data import LeaseData
import unittest


class TestLeaseData(unittest.TestCase):

    def setUp(self):
        self.data = LeaseData()
        self.data.read_csv("./data/test.csv")

    def test_prep_report1(self):
        self.data.prepare_report(1)
        expected = [4, 3, 2, 1, 5, 6, 7, 8, 9]
        self.assertListEqual(self.data.report_data, expected)

    def test_prep_report2(self):
        self.data.prepare_report(2)
        expected = [4, 7]
        self.assertListEqual(self.data.report_data, expected)

    def test_prep_report3(self):
        self.data.prepare_report(3)
        expected = {'tenant name 4': 1, 'tenant name 3': 3, 'tenant name 2': 2, 'tenant name 1': 3}
        self.assertDictEqual(self.data.report_data, expected)

    def test_prep_report4(self):
        self.data.prepare_report(4)
        expected = [2, 6, 8]
        self.assertListEqual(self.data.report_data, expected)

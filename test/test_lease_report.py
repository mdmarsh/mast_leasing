from mobilelease.lease_data import LeaseData
from mobilelease.lease_report import LeaseReport
from .test_cli import IoBaseTestCase
import sys


class TestLeaseReport(IoBaseTestCase):
    """Print tests
    Not designed to toughly test out put as that can be seen and would increase
    maintenance of test cases but might be required in some applications.

    These tests detect correct report is produced by title.
    Except for report 3 they also check last output is as expected so as to give
    an alert if something has changed.  For report 2 that includes the total

    Possible improvements if required:
    Could have detected length of output and tested for specific
    content based on test.csv

    report 3 should do an additional check - note dictionary output
    means random order - might be nicer to sort output but was not a requirement.
    Probably better to produce a better report rather than test random order.

    Note: the records selected are tested in test_lease_data

    """

    def setUp(self):
        self.data = LeaseData()
        self.data.read_csv("./data/test.csv")
        self.report = LeaseReport(self.data)

    def test_print_report1(self):
        self.stub_stdout()
        self.data.prepare_report(1)
        self.report.print(1)
        # look for -- Report 1 --  to show correct report printed
        # look for 100 at end of report
        op = sys.stdout.getvalue().strip()
        self.assertEqual("-- Report 1 --", op[0:14])
        self.assertEqual("100", op[-3:])

    def test_print_report2(self):
        self.stub_stdout()
        self.data.prepare_report(2)
        self.report.print(2)
        # look for -- Report 2 --  to show correct report printed
        # look for 119.1 the total at end of report
        op = sys.stdout.getvalue().strip()
        self.assertEqual("-- Report 2 --", op[0:14])
        self.assertEqual(" 119.1", op[-6:])

    def test_print_report3(self):
        self.stub_stdout()
        self.data.prepare_report(3)
        self.report.print(3)
        # look for -- Report 3 --  to show correct report printed
        # As this is a dictionary the order is not defined so do not test end
        op = sys.stdout.getvalue().strip()
        self.assertEqual("-- Report 3 --", op[0:14])



    def test_print_report4(self):
        self.stub_stdout()
        self.data.prepare_report(4)
        self.report.print(4)
        # look for -- Report 4 --  to show correct report printed
        # look for 3 at end of report
        op = sys.stdout.getvalue().strip()
        self.assertEqual("-- Report 4 --", op[0:14])
        self.assertEqual("3", op[-1:])
